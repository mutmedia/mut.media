import preprocess from "svelte-preprocess"
import path from "path"
import adapter from "@sveltejs/adapter-static"

/** @type {import('@sveltejs/kit').Config} */
const config = {
  preprocess: preprocess(),
	kit: {
		// hydrate the <div id="svelte"> element in src/app.html
		target: '#svelte',
    adapter: adapter({
      pages: "public",
      assets: "public"
    }),
    hydrate: true,
    router: false,
		vite: {
      resolve: {
        alias: {
          "@lib": path.resolve("./src/lib")
        }
      }
    }
	}
};

export default config;
