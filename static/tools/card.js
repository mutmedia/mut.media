export default class Card {
    id
    name

    constructor(id, name) {
        this.id = id
        this.name = name
    }
}
