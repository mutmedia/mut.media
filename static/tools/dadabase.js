import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.4/firebase-app.js'
import { collection, getFirestore, getDocs, doc, updateDoc, addDoc, getDoc, arrayUnion, arrayRemove } from 'https://www.gstatic.com/firebasejs/9.8.4/firebase-firestore.js'

import Card from "./card.js"
import { Contest, Match } from './contest.js'

const firebaseConfig = {
  apiKey: "AIzaSyDWaiNt6heh0hce3mjtaW6qh3k8eN-bZiM",
  authDomain: "trueskillstuff.firebaseapp.com",
  projectId: "trueskillstuff",
  storageBucket: "trueskillstuff.appspot.com",
  messagingSenderId: "1088316405192",
  appId: "1:1088316405192:web:c4a07ed0ccf35be8173cfd"
};

// Card Converter
const cardConverter = {
    toFirestore: (contest) => {
        return {
            name: contest.name,
        };
    },
    fromFirestore: (snapshot, options) => {
        const data = snapshot.data(options);
        return new Card(snapshot.id, data.name, data.tags);
    }
};

// Contest Converter
const contestConverter = {
  toFirestore: (contest) => {
      return {
          title: contest.title,
          cardRanks: contest.cardRanks,
          cardMatches: contest.cardMatches,
          categories: contest.categories
          };
  },
  fromFirestore: (snapshot, options) => {
      const data = snapshot.data(options);
      return new Contest(snapshot.id, data.title, data.cardRanks, data.cardMatches, data.categories);
  }
};

class Database {
  db

  constructor() {
    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    this.db = getFirestore(app);
  }

  async updateContest(contest){
    const cardRef = doc(this.db, "contests", `${contest.id}`);
    await updateDoc(cardRef, {
        title: contest.title,
        cardRanks: contest.cardRanks,
        cardMatches: contest.cardMatches
    });
  }

  async updateContestMatches(contest, match){
    const cardRef = doc(this.db, "contests", `${contest.id}`);
    await updateDoc(cardRef, {
        cardMatches: arrayUnion({winner: match.winner, loser: match.loser})
    });
  }

  async createContest(name, password = false, categories = []){
    const ref = collection(this.db, "contests").withConverter(contestConverter)
    await addDoc(ref, {title: name, cardRanks:[], cardMatches:[], categories: categories || [], password: password})
  }

  async getContests() {
    const ref = collection(this.db, "contests").withConverter(contestConverter)
    const querySnapshot = await getDocs(ref)

    const contests = []
    querySnapshot.forEach((doc) => {
      contests.push(doc.data())
    })

    return contests
  }

  async createCategory(category="space-names"){
    const ref = collection(this.db, "cards")
    await addDoc(ref, {id: category, all: []})
  }

  async createCard(name, id, category="space-names"){
    const cardRef = doc(this.db, "cards", category)
    await updateDoc(cardRef, {
        all: arrayUnion({name: name, id: id})
    });
  }

  async getAllCards(categories) {
    const ref = doc(this.db, "cards", categories.length > 0 ? categories[0] : "space-names")
    const querySnapshot = await getDoc(ref);
    const cards = [];
    const rawCards = querySnapshot.data()
    console.log(rawCards)
    rawCards.all.forEach((card) => {
      cards.push(card);
    })
    return cards
  }
}

export default new Database()

