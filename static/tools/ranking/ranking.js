import { rating, rate, ordinal, predictWin } from '../../lib/openskill.js/index.js'

import { Contest, Match } from '../contest.js'
import Card from '../card.js'

import ddb from '../dadabase.js'

window.ddb = ddb

let contests = [];

// Actual execution of stuff
let card1 = null
let card2 = null
let contest = null

const $contestTitle = document.getElementById("contest-title");
const $contestResults = document.getElementById("contest-results");
const $contestInfo = document.getElementById("contest-info");
const $cardMatch = document.getElementById("card-match");

let showRatings = false
const $toggleMatch = document.getElementById("toggle-match")
$toggleMatch.addEventListener("click", e => {
  showRatings = !showRatings
  $contestResults.classList.toggle("hidden", !showRatings)
  $cardMatch.classList.toggle("hidden", showRatings)
  $toggleMatch.innerHTML = showRatings
    ? "back to match"
    : "show ratings"
})

const $contestSelect = document.getElementById("contest-select");
$contestSelect.addEventListener("change", event => {
  const contestTitle = $contestSelect.value
  SetContestByTitle(contestTitle)
  StartContest()
})

const $btn_create_thing = document.getElementById("add-thing");
const $thing_name = document.getElementById("thing-name");
const $thing_confirmation = document.getElementById("thing-confirmation");
$btn_create_thing.addEventListener("click", async () => {
  const title = $thing_name.value
  if (title) {
    await ddb.createCard(title, title)
    $thing_name.value = ""
    $thing_confirmation.classList.remove("thing-confirmation-animation");
    void $thing_confirmation.offsetWidth;
    $thing_confirmation.classList.add("thing-confirmation-animation");
    $thing_confirmation.innerHTML = `${title} added!`
  }
})

const $currentContest = document.getElementById("current-contest");

const $card1 = document.getElementById("card1");
const $card1Name = document.getElementById("card1-name");
$card1.addEventListener("click", () => {
  RunMatchForContestAndUpdate(card1, card2)
})

const $card2 = document.getElementById("card2");
const $card2Name = document.getElementById("card2-name");
$card2.addEventListener("click", () => {
  RunMatchForContestAndUpdate(card2, card1)
})

function RunMatchForContestAndUpdate(winner, loser) {
  const m = new Match(winner.id, loser.id)
  contest.addMatch(m)
  contest.runMatch(m)
  ddb.updateContestMatches(contest, m);
  RenderContestData(contest)
  StartContest()
}

async function RunRandomContest() {
  const title = contests[Math.floor(Math.random() * contests.length)].title
  SetContestByTitle(title)
  RenderContestData(contest)
  await StartContest()
}

async function StartContest(){
  [card1, card2] = await contest.getNextMatch(allCards);
  $contestTitle.innerHTML = contest.title;
  $card1Name.innerHTML = card1.name
  $card2Name.innerHTML = card2.name
}

async function SetContestByTitle(title) {
  contest = contests.find(c => c.title === title)
  window.location.hash = title
  $contestSelect.value = contest.title
  $contestInfo.innerHTML = `${contest.cardMatches.length} matches total`
  RenderContestData(contest)
}


async function GetContests(){
  contests = await ddb.getContests()

  while ($contestSelect.lastChild) {
    $contestSelect.removeChild($contestSelect.lastChild);
  }

  // render them
  for (let contest of contests) {
    const title = contest.title;
    const $option = document.createElement("option")
    $option.value = contest.title
    $option.innerText = `${contest.title} (${contest.cardMatches.length})`

    $contestSelect.appendChild($option)
  }
}

function RenderContestData(contest){
  while ($currentContest.firstChild) {
    $currentContest.removeChild($currentContest.lastChild);
  }

  const $table = document.createElement("table");
  const $thead = document.createElement("thead");
  $table.appendChild($thead)
  const $headrow = document.createElement("tr");
  $thead.appendChild($headrow)
  $table.classList.toggle("contest-table", true)

  const headers = {}

  let meaningfulRanks = contest
    .cardRanks
  meaningfulRanks.sort((a, b) => ordinal(b.rating) - ordinal(a.rating))
  meaningfulRanks = meaningfulRanks.filter(rank => allCards.find(card => card.id === rank.id))
  const champion = meaningfulRanks[0]
  for (let i in meaningfulRanks) {
    const $row = document.createElement("tr");
    $row.classList.toggle("contest-row", true)
    $table.appendChild($row);

    function createCell(header, $child)
    {
      const $cell = document.createElement("td")
      if(headers[header] == null) {
        headers[header] = true
        const $headcell = document.createElement("th")
        $headcell.innerHTML = header
        $headrow.appendChild($headcell)
      }
      $cell.classList.toggle("contest-cell", true)
      $cell.appendChild($child)
      $row.appendChild($cell)
    }

    function createTextCell(header, text) {
      const $el = document.createElement("span")
      $el.innerHTML = text
      createCell(header, $el)
    }

    const card = allCards.find(c => c.id === contest.cardRanks[i].id)

    createTextCell("rank", i)
    createTextCell("name", card.name)

    const rating = contest.cardRanks[i].rating
    createTextCell("rating", ordinal(rating).toFixed(2))
    // createTextCell("rating mean", rating.mu.toFixed(2))
    createTextCell("variance", rating.sigma.toFixed(2))
    const matchup = predictWin([[rating], [champion.rating]]).map(c => (c* 100).toFixed(0))
    createTextCell("matchup against #1", `${matchup[0]}-${matchup[1]}`)

    predictWin

    // // for showing all the contests in a big table
    // for(let c of contests.filter(c => c.cardRanks)) {
    //   if(c.title === contest.title) continue
    //   const r = c.cardRanks.find(r => r.id === card.id)
    //   const value = (r && ordinal(r.rating).toFixed(2)) || "---"
    //   createTextCell(`rating (${c.title})`, value)
    // }
  }

  $currentContest.appendChild($table);
}

function DisplayContest() {

}

function DisplayCreateContest() {

}

let allCards = null
async function main() {
  const hash = window.location.hash.slice(1).replaceAll("%20", " ")
  const contestTitle = hash

  if(contestTitle) {
    await GetContests();
    const contest = contests.find(c => c.title === contestTitle)

    allCards = await ddb.getAllCards(contest.categories || ["space-names"]);

    DisplayContest()
    if(contest) {
      SetContestByTitle(contest.title)
      StartContest()
    }
  } else {
    DisplayCreateContest()
  }
}

main()
