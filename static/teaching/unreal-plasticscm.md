How to setup Plastic SCM in Unreal Engine
===

setup a Plastic SCM account at [https://www.plasticscm.com/](https://www.plasticscm.com/)

Plastic SCM organization
---

Plastic SCM has a limit of 3 people per team for free, one person in the group will have to set up an **organization** in Plastic SCM cloud, by pressing one of the many TRY NOW buttons in the page it should open the following form:

![](img/2022-09-13-16-13-01.png)

adding users
---

go to the [Plastic SCM dashboard](https://www.plasticscm.com/dashboard)

![](img/2022-09-13-16-05-02.png)

Go to Cloud>Configure>Users and groups
![](img/2022-09-13-16-06-39.png)

add new user, and then type the emails of your other team members. (i would recommend also adding them as admins so they can also do work on this if needed later)

each person can now go to their email to see the acceptance email, click and close the link, and they should be able to see the organization in their own dashboard.

first unreal setup
---

the basic idea of Plastic SCM is that you have all the files for your game in the cloud and there's nice features to make it so you can collaborate with such files without gettin in each other's way. your shared project is also known as a *repository*

so first step you'll need to create your initial unreal project, ideally a blank project, to minimize its size, and make it a Plastic SCM repository.

make sure you installed Plastic SCM. if you had Unreal open before, restart Unreal.

after installing Plastic SCM, go to Unreal, click on the bottom right corner where it says: "source control OFF" and it will open the source control menu.

![](img/2022-09-14-23-11-51.png)

select PlasticSCM from the dropdown and change the server address port to \<organization\>@cloud. you can leave the rest as is

// image here of the source control menu

now unreal will start sending your files into Source Control, this might take a while.

![](img/2022-09-13-16-21-16.png)

once thats done you are set up! and the other people in your group should be ready to get the repository in their own computers!

getting the repository
---

if you are one of the users invited from above, you will want to download the repository from Plastic SCM cloud. here's how you do it.

open the PlasticSCM program, you can open it in "simple" mode (Gluon). log in to the program, and you should see the repository there, press the `Download repository` button to get the files to your computer!

![](img/2022-09-14-23-02-18.png)

choose the name of the workspace you want (default to just the repo name) and where you want to install it.

![](img/2022-09-14-23-03-53.png)

then click `configure`, select all and press `Apply` to get all the files

![](img/2022-09-14-23-32-42.png)
![](img/2022-09-14-23-41-06.png)

<!-- ![](img/2022-09-14-23-04-34.png) -->

now you should have all the local files of the game on your computer! you can go to Unreal and press browse and find the location where you downloaded the project from!

[](img/2022-09-14-23-06-19.png).

press
with the project open, you now want to also connect it to source control, just like the first person did, so press the `Source Control Off` button on the bottom right.

![](img/2022-09-14-23-11-51.png)

choose Plastic SCM from the providers

![](img/2022-09-14-23-12-54.png)

this time it should not show the complicated menu, since this project is already related to Plastic SCM, so you can just press `Accept Settings`





you are now ready to start sharing files!

choose p

using Plastic SCM
---

the way this works is that you need to 'check out' any assets you want to work on, to make sure there are no conflicts with anyne else working on them. the cool thing is that unreal is intergrated with PlasticSCM and will let you know that you are checking out a file when you try changing anything.

![](img/2022-09-15-00-26-09.png)


notes
===

rememeber that you have a 5gb repository size limit for Plastic SCM, so don't add too many files to the project.
