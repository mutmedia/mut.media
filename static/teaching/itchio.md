# itch.io tutorial

### Table of contents
1. [uploading](#uploading)
2. [collaborating](#collaborating)

## uploading your game <a name="uploading"></a>


make your account at: https://itch.io/register

after logging in you can go to your name on the top right, and there's a menu, choose `upload new project`

<img src="img/2022-12-12-15-19-37.png" width="100">


a page like this should show up

<img src="img/2022-12-12-15-23-31.png" width="800">

fill all the required fields as you wish. some notes:
- you can make the url shorter than the games name
- i usually like playing around with interesting tags. if you game has any close resemblance to horror, i'd add horror, pretty popular
- kind of project should be html if you are submitting a web game, but thats a different tutorial
- maybe nothing bad in leaving a suggested donation, you put labor into this game and maybe someone will be happy to donate
- for details, you can get fancier if you click the button \<\> and know html. also maybe a different tutorial though.
- custom noun is a funny one, if you don't want your thing to be called a game

after you do all of this, the last option is visibility, which has to be a draft for the first time you save. so just press `save & view page`

![](img/2022-12-12-15-28-37.png)

now you have your page! you can edit the general look of it by pressing `edit theme`. i'd really recommend making your page not look like the default page, change the font, change the colors. it makes it look intentional and more exciting.

now you can go back to `edit game` and make your game either `restricted` (if you want only people with a password to play it) or `public` (so that everyone can play it)

congrats, you now have an itch page!

## how to share it with collaborators <a name="collaborating"></a>

from the game's page, you can now click in `analytics` on the top of the game's page to open the dashboard for this game. it shows you a bunch of interesting things for your game

![](img/2022-12-12-15-36-23.png)

for now, we care on clicking on `more>admins`

<img src="img/2022-12-12-15-36-53.png" width="100">


on the username or profile url you can type your collaborators itch username

<img src="img/2022-12-12-15-38-04.png" width="300">

and it will show you a link you have to send to that person so that they can join the project:

<img src="img/2022-12-12-15-38-58.png" width="300">

and an entry for a new project admin:

<img src="img/2022-12-12-15-39-54.png" width="300">

click on the `display as contributor` checkbox if you want their name to show up as one of the creators (ie: `how to submit a game to itch by mut, frog`)

send the link to your collaborator, and on their end, when they open the link it should look like this:

<img src="img/2022-12-12-15-42-18.png" width="300">

once they click accept, they are in and can also make changes to the game and will show up as a collaborator.

make sure to also add credits to your game page itself as well to help with people finding the collaborators.