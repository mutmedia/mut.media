const collaborators = {
  ali: {
    name: 'ali',
    // url: 'ali'
  },
  ats: {
    name: 'ats',
  },
  beau: {
    name: 'Beau McGhee'
  },
  brin: {
    name: 'brin',
    url: 'https://brin.neocities.org/'
  },
  camie: {
    name: "Camille Vogley-Howes",
  },
  dannyhawk: {
    name: "Danny Hawk",
    url: "https://dannyhawk.com/"
  },
  dh: {
    name: 'd.h.',
    url: 'https://hyperlibrary.itch.io/'
  },
  didi: {
    name: 'minhoca2d',
    url: 'https://www.instagram.com/minhoca2d/'
  },
  fabio: {
    name: 'Fabio Senna'
  },
  fran: {
    name: 'fran',
    url: 'https://frojo.itch.io/'
  },
  gurnburial: {
    name: 'gurn group',
    url: 'https://gurnburial.itch.io/'
  },
  ill: {
    name: 'isabella',
    url: 'https://isabellabs.itch.io/'
  },
  jen: {
    name: 'jen',
    url: 'https://steamedbunnies.itch.io/'
  },
  joey: {
    name: 'joey',
    url: 'https://joeyschutz.com/'
  },
  jude: {
    name: 'jude',
  },
  julian: {
    name: 'Julian Spinelli',
    url: 'https://julianspinelli.me/'
  },
  jwhop: {
    name: 'jonny hopkins',
    url: 'https://jwhop.itch.io/'
  },
  kosha: {
    name: 'KOSHA',
    url: 'https://kosha.hotglue.me/'
  },
  lama: {
    name: 'lama',
    url: 'https://lamen.itch.io/',
  },
  laura: {
    name: 'Laura Reyes'
  },
  lee: {
    name: 'Lee McGirr',
    url: 'https://www.grrr.mx'
  },
  lin: {
    name: 'ailin',
    url: 'https://ailin.itch.io/'
  },
  lucas: {
    name: 'lucas o grassi',
    url: 'https://lucasograssi.itch.io/'
  },
  moll: {
    name: "moll",
    url: 'https://www.renatomoll.com.br/',
  },
  muzio: {
    name: 'Alex Muzio'
  },
  nana: {
    name: 'nana',
    url: "https://www.instagram.com/nanabttncrt/"
  },
  neder: {
    name: 'Pedro Neder'
  },
  nichole: {
    name: "nichole shinn",
    url: 'https://nicholeshinn.com/',
  },
  pai: {
    name: 'pai'
  },
  ratinho: {
    name: 'ratinho',
  },
  rook: {
    name: 'Rook Liu'
  },
  swathi: {
    name: 'Swathi Sambassivan'
  },
  tj: {
    name: 'TJ spalty'
  },
  tori: {
    name: 'Tori Smith'
  },
  ty: {
    name: 'ty',
    url: 'https://tycobbb.itch.io/'
  },
  ubz: {
    name: 'bera',
    url: "https://www.instagram.com/uberaba_/",
  },
  walid: {
    name: 'Walid Raouda'
  },

  // collectives
  boshi: {
    name: 'boshis place',
    url: 'https://boshis.place'
  },
  pondlife: {
    name: 'pondlife',
    url: 'https://pondlife.fun'
  },
  paradise: {
    name: 'paradise',
    url: 'https://paradise-collab.itch.io/',
  },
  wonderville: {
    name: 'wonderville',
    url: "https://www.wonderville.nyc/",
  },
  xinela: {
    name: 'xinela',
    url: 'https://xinela.itch.io'
  }
}

export default collaborators