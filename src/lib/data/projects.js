import collaborators from "./collaborators.js"
import c from "./collaborators.js"

const projects = [
  //y2024
  { id: "tango",
    name: "all night long",
    month: 11,
    year: 2024,
    collaborators: [c.camie],
    tags: "arcade, music, fmod, dancing, folk, experimental, rotary encoder",
    description: "dance all night long with these lovely ladies",
    awards: [
      {
        name: "accepted into Arcade Commons 2024 Brooklyn Arts Council grant",
        url: "https://arcadecommons.org/weatherstationproject/",
      }
    ]
  },
  {
    id: "farewellsummer",
    name: "farewell summer",
    tags: "curation, event, summer",
    month: 9,
    year: 2024,
    url: "https://itch.io/c/4812830/farewell-summer-2024",
    collaborators: [c.joey, c.boshi],
    description: "a collection of games celebrating summer vibes, to celebrate the end of summer"
  },
  { id: "2hrgamejamfolder",
    name: "2 Hr GameJam Club September 2024",
    collaborators: [c.wonderville],
    month: 9,
    year: 2024,
    tags: "jam, workshop, presentation",
    url: "https://itch.io/jam/2-hr-gamejam-club-september-2024",
    description: `Make a game in 2 hours at Wonderville in Brooklyn!

Each month, we will make short games using simple to use game engines. This is great for beginners who want to make AND FINISH their first game! This is also great for experts who want to flex their skills and meet new people.

This month we are making games with files and folders.`
  },
  {
    id: "plunderjam 2024",
    name: "plunderludics workshop & jam @ boshi's place",
    url: "https://itch.io/jam/plunderludics-workshop-jam",
    tags: "plunderludics, mario, nes, 360 screen, comission",
    month: 7,
    year: 2024,
    collaborators: [c.jwhop, c.gurnburial],
    description: `organized a workshop followed by a weeklong jam at boshi's place where people made plunderludic games.

> did you ever consider the possibility of making playable videogame collages? the plunderludics working group has been creating tools to make such things more easily come to our workshop and jam this coming week and make your own thing!
>
> we will kick off the jam with a workshop and in person work time with us helping you out then you will have a week to work on your game and on the next sunday, more in person work time, culminating on a moment we can all showcase our work!

# Theme Games

  - Crash Bandicoot (PS1)
  - Glover (N64)
  - Ogre Battle 64: Person of Lordly Caliber (N64)`
  },
  { id: "rainyday",
    name: "games for a rainy day launch party @ boshis place",
    tags: "event, producer, zines, performance, free improv",
    day: 19,
    month: 4,
    year: 2024,
    url: "https://boshis.place/events/24-04-19-games-for-a-rainy-day",
    collaborators: [ {name: "Syd", url: "https://www.sydastry.com/"}, c.nichole]
  },
  { id: "reimaginezine",
    name: "re: imagine the character (zine version)",
    tags: "imagination, zine, riso, mario",
    month: 3,
    year: 2024,
    // TODO!!
  },
  { id: "mariotag",
    name: "mario tag",
    month: 2,
    year: 2024,
    url: 'https://plunderludics.github.io/works/mario%20tag.html',
    collaborators: [c.jwhop, c.gurnburial],
    description: `The Super Mario franchise, originating from Nintendo in 1985, is a seminal work in video game history. Set in the whimsical Mushroom Kingdom, players assume the role of Mario, joined by his brother Luigi, on quests to rescue Princess Peach from the clutches of Bowser. The game mechanics, featuring platforming challenges and strategic power-up items like the Super Mushroom and Fire Flower, offer a diverse and engaging experience. Its enduring popularity and scholarly interest make it a pivotal subject in the gaming industry.

    Exploring techniques the artists call "plunderludics," "Mario Tag" is a remix of sorts. By creating a custom emulator that changes how the original game is presented, while keeping the software untouched, "Mario Tag" is a fresh twist on Super Mario Bros: two players embody the iconic plumber, Mario, at the same time, engaging in an electrifying chase through different levels of the Mushroom Kingdom. Adding to the excitement is the 360-degree screen, allowing the pursuit to extend indefinitely. Each time a player dies, the world morphs randomly, injecting unpredictability into the gameplay. The competing Marios try to hold on to a 'ball' for the longest amount of time to win. Experience the fusion of fun and innovation in "Mario Tag," where the pursuit never cease!`,
    awards: [
      {
        name: "comissioned by University of Hong Kong",
        year: 2024
      }
    ],
    exhibitions: [
      {
        name: "Art Basel Hong Kong 2024 @ Hong Kong University",
        url: "https://plunderludics.github.io/devlogs/2024-03-27%20Looped%20Play%20(art%20basel).html",
        year: 2024
      },
      {
        name: "boshi's 1-year anniversary @ boshi's place",
        url: "https://boshis.place/events/24-04-06-boshis-first-birthday",
        year: 2024
      },
    ],
    mentions: [
      {
        name: 'p-articles',
        url: "https://p-articles.com/critics/4470.html",

      }
    ],
  },
  //y2023
  { id: 'spontaneous-composition',
    name: 'spontaneous composition',
    month: 9,
    year: 2023,
    collaborators: [c.camie],
    tags: "event, music, performance, game pieces, graphic scores, folk games, fluxus, metagaming",
    exhibitions: [
      {
        name: "boshi's backyard",
        url: 'https://www.instagram.com/p/CxQ1evju_c1/',
        month: 9,
        year: 2023,
      },
    ]
  },
  { id: 'folk-drinking-game-jam',
    name: 'folk drinking game jam',
    url: 'https://www.instagram.com/p/CuODlWKNlen/',
    month: 7,
    year: 2023,
    tags: "folk games, event, game jam, drinking games",
  },
  { id: 'cube-game',
    name: "cube game",
    url: 'https://mutmedia.itch.io/cubegame',
    month: 7,
    year: 2023,
    tags: "flickgame, gift",
  },
  { id: 'continental-breakfast',
    name: 'continental breakfast: select flash games from 2001-2008',
    month: 6,
    year: 2023,
    tags: "art, curation, event, flash games, history",
    description: "Flash games are a time capsule of culture, aesthetics, platforms, games, animation, politics, teenagerdom, and of 2000s internet itself. We did a survey of flash games from 2001-2008 and presented a cross section of them."
  },
  { id: 'circling-back',
    name: "circling back",
    url: 'https://mutmedia.itch.io/circling-back',
    month: 5,
    year: 2023,
    tags: "gurnlike, music, gift",
  },
  { id: 'pique-esconde',
    name: "pique esconde",
    url: 'https://mutmedia.itch.io/esconde-esconde',
    month: 5,
    year: 2023,
    tags: "bitsy",
    mentions: [
      {
        name: "weird fucking games",
        url: "https://twitter.com/wf__games/status/1667237885370675201"
      }
    ],
  },
  {  id: 'adjunct-nyu-fall2023',
    name: "adjunct professor @ nyu game center - Game Design 1 (graduate)",
    year: 2023,
    tags: "teaching, game design, analog, folk games",
    description: "graduate level introductory class on designing games, from a analog direction.",
  },
  {  id: 'adjunct-pratt-fall2023',
    name: "adjunct professor @ pratt - Collaborative Game Design",
    year: 2023,
    tags: "teaching, game design, analog, folk games",
    description: "undergraduate introductory class on designing games through collaboration between multiple developers. both a practical and theoretical class understanding games from a mechanical, systemic and narrative perspective, while exploring a wide range of references and creating, playtesting and developing multiple games.",
  },
  {  id: 'adjunct-pratt-spring23',
    name: "adjunct professor @ pratt - Object Oriented Programming",
    // month: 5,
    year: 2023,
    tags: "teaching, game development, programming, c#",
    description: "undergraduate introduction to programming class focused on games. developed my own syllabus in which students develop a simple terminal oriented language with object oriented structures mirroring component systems in mainstream engines. also adding a creative practice on the side for the students to explore their learnings by creating art with processing",
  },
  {  id: 'adjunct-nyu-spring23',
    name: "adjunct professor @ nyu game center - Intro to Game Dev",
    //month: 5,
    year: 2023,
    tags: "teaching, game development, game maker",
    description: "undergraduate class focusing on a generalist approach to video game development, programming, visual art, sound, game feel, production, &c; using game maker",
  },
  {  id: 'adjunct-parsons-spring23',
    name: "adjunct professor @ parsons - Game Craft",
    //month: 5,
    year: 2023,
    tags: "teaching, game design, analog, folk games",
    description: "undergraduate introductory class on game design and analysis. brough my own perspective as a folk game enjoyer and play-centric designer to write a syllabus from scratch that focused on fast iteration on projects and constant playing and analysing of games, from folk games, to table top rpgs, to board games and larps",
  },
  { id: 'zinejam',
    name: 'zine jam #0',
    collaborators: [
      c.jen, c.boshi
    ],
    // month: 3,
    year: 2023,
    tags: "event, jam, zines, physical",
    description: "the first iteration of a series of events @ space with no name yet, where people come over and we spend an afternoon making jams about a specific theme"
  },
  {
    id: 'spring-salad',
    name: 'spring salad: a plunderludic gathering',
    collaborators: [
      c.gurnburial,
      c.jwhop,
      c.boshi
    ],
    // month: 2,
    year: 2023,
    tags: "event, curation, gallery, art",
    description: `show of mine, gurn group's and jonny's current exploration in plunderludics and adjacent work done by other artists over the years, with works by AP thomson & Bennett Foddy, Alistair Aitcheson, Cory Archangel, Lawra Suits Clark, Patrick Lemieux & Stephanie Boluk`,
  },
  {
    id:'tapestry',
    name: "tapestry (tapeçaria) #4",
    month: 2,
    year: 2023,
    tags: "plunderludics, n64, psx, walking sim, gamefeel, hauntology",
    description: `multiple third person games from 5th generation consoles (psx, n64), cut, sampled, limited and blended as such to allow for textural exploration.

a plunderludic experiment that plays with memory and nostalgia and the meaning of moving within and around videogames. exploring the relation of the infinite possibilities of plays within constrained virtual spaces, which sometimes might mean pushing your avatar into a wall until it becomes something else`,
    awards: [
      {
        name: "honorable mention @ A MAZE 2024",
        url: "https://2024.amaze-berlin.de/mentions/tapestry/",
        month: 5,
        year: 2024,
      },
    ],
    exhibitions: [
      {
        name: 'Festival ECRÃ 2023',
        url: 'https://www.festivalecra.com.br/7fecragames/tapecaria',
        month: 7,
        year: 2023,
      },
      {
        name: "space with no name yet's spring salad: a plunderludic gathering",
        url: 'https://plunderludics.github.io/wiki/?path=%2Fwiki%2Fdevlogs%2F2023-02-25%2520spring%2520salad.html/',
        month: 2,
        year: 2023,
      },
      {
        name: "A MAZE 2024",
        url: "https://2024.amaze-berlin.de/mentions/tapestry/",
        month: 5,
        year: 2024,
      },
      {
        name: "Plundergames @ LIKELIKE",
        url: "http://likelike.org/2024/05/29/plundergames/",
        month: 6,
        year: 2024
      }
    ]
  },
  //y2022
  // TODO: throbber / unblockable entertainement
  {  id: 'adjunct-pratt-production2',
    name: "adjunct professor @ pratt - Game Production 2",
    //month: 5,
    year: 2022,
    tags: "teaching, game production, object oriented programming",
    description: "working close with the students over a course of a semester to help them develop an entire game from start to finish while learning production and collaboration practices"
  },
  {
    id: 'businessman',
    name: 'businessman',
    year: 2022,
    tags: "art, videogame, discone, thirdperson, comission, plundercore",
    url: 'https://mutmedia.itch.io/businessman',
    description: "a group of businessman just arrived in delfino square, and they want to make sure they own all the coins around here. traditional laws of physics don't apply for business. a videogame in the discone universe, exploring ideas of movement through space withing digital worlds and plundercore aesthetics of repurposing assets from existing games into new contexts.",
    awards: [
      {
        title: 'commission',
        giver: `indiepocalypse issue #36`,
        url: `https://pizzapranks.itch.io/indiepocalypse-36`
      }
    ],
    press: [
      {
        title: `Indiepocalypse Double Feature: Traversing Borrowed Worlds`,
        url: "https://pizzapranks.com/indiepocalypse-double-feature-traversing-borrowed-worlds/"
      },
      {
        title: `JohnLee Cooper 10 Indiepocalypse Picks`,
        url: "https://pizzapranks.com/johnlee-coopers-10-indiepocalypse-picks/"
      }
    ],
  },
  {
    id: 'plunderludics-wg',
    name: 'plunderludics working group',
    year: 2022,
    tags: "art, research, metagaming, plunder, plunderludics, videogames, collage",
    collaborators: [
      c.gurnburial,
      c.jwhop
    ],
    url: "https://plunderludics.github.io",
    presentations: [
      {
        name: 'workshop @ Hand Eye Society SUPERFestival 2023',
        month: 11,
        year: 2023,
      },
      {
        name: "workshop @ boshi's place",
        url: "https://www.youtube.com/watch?v=gwgu7M3NGxo",
        month: 7,
        year: 2024,
      },
      {
        name: 'opening @ Festival ECRÃ 2023',
        url: 'https://www.twitch.tv/videos/1865261627',
        month: 7,
        year: 2023,
      },
    ],
    exhibitions: [
      {
        name: "babycastles' maybe the last event ever maybe",
        url: 'https://withfriends.co/event/15353988/party_on_the_3rd',
        month: 12,
        year: 2022,
      },
    ],
  },
  {
    id: 'posersonly',
    name: 'Posers Only!',
    month: 7,
    year: 2022,
    tags: "art, curation, event",
    collaborators: [
      c.wonderville
    ],
    url: "https://www.wonderville.nyc/events/posers-only",
    description: `event at local new york city bar displaying multiple games related to skateboarding culture`,
    exhibitions: [
      {
        name: 'wonderville',
        url: 'https://www.wonderville.nyc/events/posers-only',
        month: 7,
        year: 2022,
      },
    ]
  },
  {   id: 'mspaint',
    name: 'mspaint',
    year: 2022,
    tags: "art, mspaint, drawing, illustration, personal",
    url:'./mspaint',
    description: `collecting all mspaint drawings i ever do and remember to save, a lot of math sketches, a bunch of drawings, some clipboard pictures, memes, and stuff im not even sure about`
  },
  {   id: 'real-colors',
    name: 'real colors',
    year: 2022,
    tags: "art, color, discord, folk games, collab",
    url:'https://youcanneverleave.art/real-colors/',
    collaborators: [
      c.paradise
    ],
    description: `inspired by bereal, a discord channel where people post colors they find. archived and collected into a website`
  },
  {   id: 'hungry',
    name: 'i\'m hungry',
    year: 2022,
    tags: "art, videogames, toy, jam, collection, mario, models-resource, collab, paradise, experimental",
    collaborators: [
      c.paradise
    ],
    description: `a game made from the translation of its description, for the [i'm hungry game jam](https://itch.io/jam/im-hungry/)

Vous contrôlez un petit rennes qui doit attraper le plus de nourriture possible, il s'agit de pain et de mûres. Plus vous attrapez de, plus la nourriture va vite et plus il est compliqué de l'attraper. En haut à droite, vous voyez qu'il y a votre tableau de bord, chaque aliment pêché vaut un point. Et en haut à gauche, il y a un panneau qui vous montre combien d'aliments vous n'avez pas attrapés. Attention ! au bout d'une dizaine d'aliments non attrapés vous perdez et le jeu affiche alors « GAME OVER ! », alors il faut appuyer sur enter pour recommencer.

Les commandes sont : la flèche droite pour se déplacer vers la droite, la flèche gauche pour se déplacer vers la gauche et la flèche vers le haut pour sauter. C'est si simple !
    `,
    url: "https://mutmedia.itch.io/yoshi-is-hungry",
    itchId: 1394290,
  },
  {   id: 'magicalparadisetrain',
    name: 'Magical Paradise Train',
    year: 2022,
    tags: "videogames, microgames, collection, collab, skategames",
    collaborators: [
      c.brin, c.paradise
    ],
    description: `a collection of microgames made with the paradise game making community to commemorate 100 users on the discord. its very hard to make 100 games, but we might as well try. made coin scratching game with brin.`,
    url: "https://paradise-collab.itch.io/magical-paradise-train",
    itchId: 1521777,
  },

  {   id: 'paradisezine1',
    name: 'Paradise Zine Issue 1',
    year: 2022,
    tags: "writing, videogames, games, collab, zine, skategames, imagination",
    collaborators: [
      c.paradise
    ],
    description: `a collection of writing and art done by/with the game making community of paradise, i wrote the [skate games essay](./skategames) and helped edit some of the other essays.`,
    url: "https://paradise-collab.itch.io/paradise-zine-1",
    itchId: 5990128,
  },
  {  id: 'nyu-mfa',
    name: "mfa @ nyu game center",
    //month: 5,
    year: 2022,
    tags: "degree, art, game design",
    description: "i'm a master of fine arts in game design!"
  },
  {  id: 'gamer-potluck',
    name: "gamer potluck",
    url: 'https://www.twitch.tv/hawkdanny/videos',
    //month: 5,
    year: 2022,
    collaborators: [
      c.dannyhawk
    ],
    tags: "streaming, game analysis, game curation",
    description: "every monday we pick 2 games for each other to play, exploring our tastes and interests while having deep (and sometimes just obnoxious) conversations about games and their making"
  },
  {   id: 'discone',
    name: 'pondlife: discone (a videogame)',
    year: 2022,
    tags: "art, discone, videogames, toy, play, playground, mmo, online, experimental, multiplayer, skategame",
    collaborators: [
      c.pondlife, c.gurnburial, c.ty, c.ali, c.fran, c.wonderville
    ],
    description: `run & jump around
a giant imponderable world
with affecting characters
at first alone
in pondlife:
discone
(a videogame)`,
    url: "https://pondlife.fun/discone",
    itchId: 1394290,
    exhibitions: [
      {
        name: "wonderville's late fall videogame showcase",
        url: 'https://www.wonderville.nyc/events/late-fall-vg-showcase-12-5-22',
        month: 12,
        year: 2022,
      },
      {
        name: "syzygy's bay area indie dev showcase",
        url: 'https://www.syzygysf.com/event-details/bay-area-indie-dev-showcase',
        month: 3,
        year: 2023
      },
      {
        name: "bar and grill with pool table @ GDC",
        url: 'https://www.eventbrite.com/e/gdc-after-party-with-indie-games-music-food-and-arcade-game-scrapeboard-tickets-588671491277',
        month: 3,
        year: 2023
      }
    ],
    mentions: [
      {
        title: `patrick lemieux's tweet`,
        url: 'https://twitter.com/alt254/status/1639473273292603392'
      }
    ],
  },
  {   id: 'dadada',
    name: 'dadada',
    year: 2022,
    tags: "cardgame, language, folkgame, coop, published",
    collaborators:[
      c.julian, c.lee, c.tj, { name: "the op games", url: "https://theop.games/" }
    ],
    description: 'a social party game designed in collaboration with TJ Spalty, Julian Spinelli, and Mut. Signed to be published by The Op (usaopoly), publishers of Monopoly, Clue, and Telestrations. expected release soon',
    // description: 'The goal of DaDaDa is to create a card game based experience that teaches players how to intuitively and collaboratively create a new language, and then successfully practice the created language to communicate new and interesting concepts to each other. DaDaDa aims to show a player how, after 10 minutes, “bipdif” can mean sun, “bipdifzoop” can mean beach, and somehow it all makes sense. Our hope is to cultivate this engaging, unique, and deep moment of ideation in players across a wide spectrum by packaging the game in an approachable and fun format.',
    relevance: 'acquired by mass market board game publisher The Op. The Op is the publisher of licensed versions of game such as Monopoly and Clue and also has an appeal in the casual market with games like Telestrations and Hues and Cues.',
    awards: [
      {
        title: "Pal Awards 2024",
        url: "https://playonwords.com/award/15826/",
        year: 2024,
      },
      {
        title: "Board Game Quest's Gen Con 2024 Staff Pick",
        url: "https://www.boardgamequest.com/gen-con-2024-recap-the-games-news-and-staff-picks/",
        year: 2024
      },
      {
        title: `selected`,
        giver: `NYU Game Center Incubator 2022`,
        year: 2022,
      },
    ],
    exhibitions: [
      {
        name: "DWNTWN BRKLYN's Willoughby Walks",
        month: 4,
        year: 2022
      },
      {
        name: "NYU Game Center Showcase 2023",
        url: "https://www.twitch.tv/videos/1805320409",
        month: 4,
        year: 2023,
      },
      {
        name: "Gen Con 2024",
        month: 8,
        year: 2024
      },
      {
        name: "Pax Unplugged 2024",
        month: 12,
        year: 2024
      }
    ],
    mentions: [

    ]
  },
  {   id: 'banana',
    name: 'oooh banana (for bananajam 2022)',
    year: 2022,
    url: 'https://mutmedia.itch.io/banana',
    description: 'game made for a [gamejam](https://itch.io/jam/bananajam/entries) with a gamefeel study group where we first spent an hour exploring the feel of real bananas, squishing, smacking, eating, twisting, &c and then made a game based on the feeling over the next meeting (try finding who didn\'t squish bananas)',
    itchId: 1552289
  },
  {   id: 'ramrush',
    name: 'ram rush',
    year: 2022,
    tags: "tabletop, dexterity, versus, 1v1",
    collaborators:[
      c.beau, c.swathi, c.rook, c.tori, c.walid
    ],
    description: "Ram Rush is a two-player real-time game where you play as two rams trying to headbutt each other off a mountain. Your ram honor is on the line, so add chips to the battle lane until one of you falls to your doom. Ready, set, GO-AT!",
    url: "https://linktr.ee/ram_rush",
    exhibitions: [
      {
        name: "DWNTWN BRKLYN's Willoughby Walks",
        month: 4,
        year: 2022
      }
    ]
  },
  {   id: 'tododia',
    name: 'tododia',
    year: 2022,
    tags: 'daily, net-art, prototype, toys, art, experimental, poetry, visual art',
    description: 'made a new webpage everyday for 40 days',
    url: './tododia/2'
  },
  {
    id: 'valentines22',
    name: 'LOVE ME, PLAY ME',
    year: 2022,
    tags: "art, curation, event, love",
    collaborators: [
      c.pondlife, c.gurnburial, c.ty, c.ali, c.fran
    ],
    url: "https://www.wonderville.nyc/events/love-me-play-me-2-17",
    description: `event at local new york city bar displaying multiple games related to valentines day`,
    exhibitions: [
      {
        name: 'wonderville',
        url: 'https://www.wonderville.nyc/events/love-me-play-me-2-17',
        month: 2,
        year: 2022
      },
    ]
  },
  // TODO: arats
  // TODO: pondlife issue 0
  {
    id: 'gitg',
    name: 'games in the grass',
    year: 2022,
    tags: "folk games, fun, play, event",
    collaborators: [
      c.pondlife, c.gurnburial, c.ty, c.ali, c.fran
    ],
    url: "https://pondlife.fun/grass/events/",
    description: `a series of events for playing and exploring folk games in public`,
    relevance: `we got interviewed by [indiecade](https://www.indiecade.com/). https://twitter.com/indiecade/status/1451924371476418561`,
    exhibitions: [
      {
        name: "DWNTWN BRKLYN's Willoughby Walks",
        month: 10,
        year: 2021
      }
    ]
  },
  // y2021
  {   id: 'dumplinglove',
    name: 'dumpling.love',
    year: 2021,
    tags: 'best, internet, net-art, anarchy, haunting, experimental, narrative, toys, meta, videogame, web',
    collaborators: [
      c.gurnburial, c.dh, c.jen, c.fran, c.ty
    ],
    description: 'a funny little website about strolling through a park in a post internet world. started as a student project in 2021, but in continuous development ever since',
    awards: [
      {
        title: `honorable mention`,
        giver: `11th AMAZE, International Game and Playful Media Festival`,
        details: `One of 15 honorable mentions out of 189 submitted games. From their website: 'A MAZE. Awards goes to the most relevant, forward-thinking interactive and playful content. We premiere the best work overall - in terms of art, innovation, music, story and interaction. The prize is dedicated to the masterpiece of the award selection that shows us a path we never walked before.'`,
        url: `https://2022.amaze-berlin.de/nominees/`
      },
      {
        title: `honorable mention for student game`,
        giver: `IGF 2022`,
        details: `Indepenent Games Festival, the biggest international festival for independent videogames. One of 8 honorable mentions out of 400 total submitted games.`,
        url: `https://igf.com/archive/2022`
      },
      {
        title: `Experimental Award`,
        giver: `RPI Game Fest 2022`,
        details: `from their website: "GameFest is an annual celebration of student creativity and innovation in the broader fields of analog and digital game design." "The competition itself is judged each year by a rotating panel of local industry experts, all of whom bring a wealth of experience from across the game development world."`,
        url: `https://gamehub.rpi.edu/gamefest/awards`,
      }
    ],
    presentations: [
      {
        name: "WordHack October 24'",
        url: 'https://youtu.be/Ypw2LYN9LXw?si=KnQ3Az6uDoR8gVrf&t=3251',
        month: 10,
        year: 2023,
      },
      {
        name: "HTTPoetics winter 24' @ sfpc",
        url: "https://sfpc.study/blog/a-poetic-web",
        month: 3,
        year: 2024
      }

    ],
    exhibitions: [
      {
        name: 'HTML review',
        url: 'https://thehtml.review/03/#parks-staff',
        month: 3,
        year: 2024
      },
      {
        name: 'Festival ECRÃ 2023',
        url: 'https://www.festivalecra.com.br/7fecragames/dumpling_love',
        month: 7,
        year: 2023,
      },
      {
        name: 'WASD Curios',
        url: 'https://www.wasdlive.com/news/j12utd8yn0uejatg8vmj3vxkxfzjlk',
        month: 4,
        year: 2023,
      },
      {
        name: 'Super FESTival',
        url: 'https://handeyesociety.com/superfestival/?passage=game10',
        month: 9,
        year: 2022,
      },
      {
        name: 'A MAZE 2022',
        url: 'https://2022.amaze-berlin.de/mentions/dumplinglove/',
        month: 5,
        year: 2022,
      },
      {
        name: 'playNYC',
        url: 'https://www.play-nyc.com/',
        month: 8,
        year: 2021,
      },
    ],
    itchId: 980212,
    url: 'https://dumpling.love',
    source: 'https://github.com/frogfrogforg/c0llage'
  },
  {   id: 'vela',
    name: 'Vela',
    url: 'https://lucasograssi.itch.io/vela',
    year: 2020,
    tags: 'best, horror, first person, game feel, candle, mood, psx, videogame',
    collaborators: [
      c.ill, c.lucas, c.kosha
    ],
    description: "a moody game about navigating a maze with a sensitive candle. inspired by the candle scene in Tarkovsky's the mirror",
    exhibitions: [
      {
        name: 'boshi presents: first annual day of dry bones',
        url: 'https://www.instagram.com/p/CygixXOOUg2/',
        month: 10,
        year: 2023,
      },
      {
        name: 'wonderville haunted arcade',
        url: 'https://www.wonderville.nyc/events/haunted-arcade-10-31',
        month: 10,
        year: 2021,
      },
    ],
    itchId: 683454
  },
  {   id: 'bruxa',
    name: 'Bruxa',
    year: 2021,
    url: 'https://xinela.itch.io/bruxa',
    tags: 'best, local multiplayer, skate, movement, level design, videogame',
    collaborators: [
      c.ats, c.kosha, c.nana, c.ubz, c.ratinho, c.xinela
    ],
    description: 'a local multiplayer control swapping skate like!',
    awards: [
      {
        title: 'grant',
        giver: `brazilian ministry of culture`,
        details: `brazilian ministry of culture grant for digital game prototypes in 2019`,
      }
    ],
    exhibitions: [
      {
        name: "Boshi's Second day of dry bones @ boshi's place",
        month: 8,
        year: 2024
      },
      {
        name: 'BRING #18 - Brasilia Indie Games Showcase',
        month: 2,
        year: 2020,
      },
    ],
    itchId: 1146879
  },
  {   id: 'vertexpaint',
    name: 'vertex paintings',
    year: 2021,
    tags: 'visual, painting, illustration',
    description:
      'digital paintings made by using vertex color painting on a triangular mesh. a very fun technique thaught by chris makris',
    imgs: ["vertex100.png", "vertex50.png", "vertex500.png"],
  },
  {   id: 'prototypes-2021',
    name: 'a game every week',
    year: 2021,
    tags: 'prototype, sketch, videogames',
    description: '13 weeks of making game prototypes. highlights include sysiphean mario golfing, some [plunderludics](#plunderludics), a sensual controller game and recovering the long lost Balrog The Frog DSi arcade game...',
    url: 'https://itch.io/c/1822256/game-a-week-2021',
  },
  {
    id: 'saint-wiggler',
    name: 'saint wiggler in the desert',
    year: 2021,
    itchId: 1208314,
    tags: 'sysiphean, golf, small, hard, desert, videogames, plundercore',
    description: 'golf in the desert',
    url: 'https://muts.itch.io/saint-wiggler-in-the-desert',
    exhibitions: [
      {
        name: 'seacave x wonderville',
        url: 'https://www.wonderville.nyc/events/seacave-wonderville',
        month: 6,
        year: 2022,
      },
    ]
  },
  {
    id: "ta-nyu",
    name: "teaching assistant @ nyu game center",
    year: 2021,
    // month: 1,
    end: 2022,
    // endmonth: 9,
    tags: "teaching, game development, game design",
    description: `worked as a teaching assistant for: intro to game development, bfa (summer 21), intermediate game design, bfa (fall 21), game studio 2, mfa (spring 22)
`
  },
  {   id: 'hugkin',
    name: 'hugkin',
    year: 2021,
    tags: 'card game, ttrpg, ccg, legacy, imaginary, cute',
    collaborators: [
      c.ty, c.jude
    ],
    description:
      `a card game about taking care of your creatures, trading them with your friends, receiving and making marks and letting go. blending the lines between collectable, role playing and memento.

here are my current hugkin:`,
    imgs: ['hugkin/muts-lucky beard.png','hugkin/muts-pantyp.png','hugkin/muts-tait.png','hugkin/muts-thinker.png','hugkin/muts-yo.png' ]
  },
  {   id: 'cdj2',
    name: 'cacadores de jam season 2',
    year: 2021,
    tags: "art, streaming, jam, collaboration, prototypes",
    url: "https://itch.io/c/1399240/cacadores-de-jam-season-2",
    collaborators: [
      c.kosha, c.lama
    ],
    description: "second 8-episode season of our weekly 4-hour gamejam stream (see cacadores de jam season 1, 2020)",
  },
  // y2020
  {
    id: 'altinha',
    name: 'super altinha soccer',
    month: 12,
    year: 2020,
    url: "https://lamen.itch.io/super-altinha-soccer",
    tags: "football, brasil, soccer, jam, play, mobile",
    exhibitions: [
      {
        name: "farewell summer @ boshi's place",
        url: "https://boshis.place/events/24-09-21-farewell-summer"
      }
    ]
  },
  {   id: 'cdj1',
    name: 'cacadores de jam season 1',
    year: 2020,
    tags: "art, streaming, jam, collaboration, prototypes",
    url: "https://itch.io/c/1261260/caadores-de-jam-season-1",
    collaborators: [
      c.kosha, c.lama
    ],
    description: "first 8-episode season of our weekly 4-hour gamejam stream. every week, me kosha, lama and sometime guests would find a gamejam that was happening on itch.io and submit a game to it made on stream. as a bonus end of season show, we did a weekend long session polishing one of the games, [super altinha soccer](https://lamen.itch.io/super-altinha-soccer)",
  },
  {   id: 'pesujo0',
    name: 'pesujo TV',
    year: 2020,
    tags: "art, streaming, collaboration, collective",
    collaborators: [
      c.kosha, c.lama, c.ubz, c.didi,
      {name: "ted", url: "https://www.instagram.com/gasparianted/"},
      {name:"CHEZZ", url: "https://www.instagram.com/chezz8000/"}
    ],
    description: "a twitch stream variety collective focused on alternative art making.",
  },
  {   id: "dog-park",
    name: "New Froggington On-Leash Recreation Area",
    tags: "art, multiplayer, folk games, funny, jank, music, mickey mousing, dog, physics",
    url: "https://gurnburial.itch.io/on-leash-recreation-area",
    collaborators: [
      c.gurnburial, c.ty, c.laura
    ],
    // month: 11,
    year: 2020,
    description: "online multiplayer physics based dog park simulation. every player has a dog that behaved by itself, wanting to interact with balls, other dogs and other players in the world. we hope your leash is working well if you want to go anywhere.",
  },
  {   id: "ldjam20202", // TODO
    name: "I made this game 20 minutes before LD jam submission time because for some reason I felt like I should",
    // month: 10,
    year: 2020,
  },
  {   id: "satyr",
    name: "Summer All the Year Round ",
    collaborators: [
      c.jen
    ],
    // month: 10,
    year: 2020,
    url: "https://steamedbunnies.itch.io/summer-all-the-year-round",
    description: "In this campy, theatrical, romantic comedy, play as the Hero, recruited to defeat Godzilla! Take his shopping list, get him gifts, make friends, and set up the townsfolk! Maybe even you will fall in love!!",
    itchId: 799648,
  },
  {   id: "visual-novelties",
    name: "visual novelties",
    // month: 9,
    year: 2020,
    description: `You just found this a visual novel I made based on Italo Calvino's if on a Winter's Night a traveler. If you know that book, you might already know the following spoilers: the text will be very meta and probably be interspersed between moments of "you", as a character reading it and short experimental texts in multiple narratives. Eventually I wish to implement a way so you can skip to the chapter you want to, just as in a real book.`,
    url: 'https://mutmedia.itch.io/visualnovelties',
    itchId: 767812,
  },
  {   id: "out-of-ctrl",
    name: "Out of Ctrl",
    // month:07,
    year:2020,
  },
  {   id: 'reimagine',
    name: 'Re: Imagine the Character',
    tags: 'best, conceptual, meta, imaginary, videogame, web, net-art',
    year: 2020,
    description: 'a videogame about how imagination is the most powerful game mechanic',
    itchId: 570269,
    exhibitions: [
      {
        name: "idle arcade @ night city games",
        url: "https://nightcity.games/2024/02/13/idle-arcade/",
        month: 2,
        year: 2024,
      },
      {
        name: `wonderville's NO CTRL show`,
        url: `https://www.wonderville.nyc/events/spring-intersnhip-showcase-2023`,
        month: 3,
        year: 2023,
      }
    ],
    mentions: [
      {
        title: "wonderville twitter",
        url: "https://twitter.com/wondervillenyc/status/1639745610155737088",
      }
    ],
  },
  // y2019
  {   id: 'nothing',
    name: 'Stumbling Upon Meaning in an Old Town Abandoned to its Fate',
    year: 2019,
    tags: 'best, narrative, experimental, poem, text-based, videogame',
    collaborators: [
      c.ill, c.lucas
    ],
    description: 'a game about how narrative can explode from a single word',
    url: 'https://mutmedia.itch.io/stumbling-upon-meaning-in-an-old-town-abandoned-to-its-fate',
    itchId: 494711,
    press: [
      {
        title: `pressover news`,
        url: "https://pressover.news/indies-pensables/stumbling-upon-narrativa-no-lineal-interactiva-directo-de-brasil/",
        year: 2023,
        month: 6,
        day: 1
      }
    ],
  },
  {   id: "alien",
    name: "Alien",
    collaborators: [
      c.ats, c.kosha, c.neder, c.xinela,
    ],
    // month:11,
    year:2019,
  },
  {   id: "bolcano",
    name: "Bolcano",
    // month: 11,
    year: 2019,
  },
  {
    id: "cowboy",
    name: "Cowboy",
    collaborators: [
      c.kosha
    ],
    // month: 6,
    year: 2019,
  },
  {
    id: "avida",
    name: "Avida",
    collaborators: [
      c.lin, c.ats, c.kosha, c.xinela
    ],
    // month: 3,
    year: 2019,
  },
  {   id: "home",
    name: "HOME",
    tags: "art, video game, recursion, arcade, jam, global game jam",
    collaborators: [
      c.muzio, {name: "Alex Kolosoff"}
    ],
    // month:01,
    year:2019,
    description: "a game where you teach blood vessels, one at a time, to move around the body to give oxigen to the parts that need it",
    url: "https://mutmedia.itch.io/home",
    itchId: 362237
  },
  {
    id: "microsoft2019",
    name: "software developer engineer @ Gears of War - The Coalition - Microsoft",
    year: 2019,
    // month: 1,
    end: 2020,
    // endmonth: 9,
    tags: "job, software engineering, programming, backend, services, stress testing, matchmaking",
    description: "working on the services team on the months leading to the release of Gears V, working on a variety of projects such as stress testing the servers to make sure they would handle the load on release day and being in conversation with the research team behind TrueSkill to create a better designed matchmaking user experience after the games's release"
  },
  // y2018
  {    id: 'natureza-morta',
    name: 'natureza morta',
    year: 2018,
    tags: "art, visual art, painting, oil, still life",
    description: "a painting i think turned out pretty good. made me certain that i can do it, and that painting is mostly a lot of work. trace trace trace.",
    imgs: ["natureza-morta.png"],
  },
  {
    id: "game-title",
    name: "My game's title here",
    tags: "art, bitsy, story, short, ludum dare, jam",
    // month: 12,
    year: 2018,
    description: "game i made for ludum dare 43, just for the sake of doing something",
    url: "https://mutmedia.itch.io/my-games-title-here",
    itchId: 340498,
  },
  {    id: 'curta-brasilia',
    name: 'vinheta 7 curta brasilia',
    tags: "art, video, rendering, animation, vfx, freelance",
    year: 2018,
    collaborators: [
      c.kosha, c.ats, c.xinela,
    ],
    description: 'a short intro video made for brasilia\'s 7th international short movie festival',
    exhibitions: [
      {
        name: "7th curta brasilia - brasilia short film festival",
        month: 11,
        year: 2018,
      }
    ],
    url: "https://www.youtube.com/watch?v=fcY8lku19UM",
  },
  {   id: "sabado-prime",
    name: "Sabado Prime",
    collaborators: [
      c.ats, c.lin, c.ill, c.nana, c.ubz, c.kosha, c.fabio, c.xinela
    ],
    // month:11,
    year:2018,
  },
  {
    id: "realistic-fighting-game",
    name: "Jogo De Luta Ultra Realista Para Fliperamas",
    description: "the most realistic fighting game i know of",
    // month: 11,
    year: 2018,
  },
  {
    id: "competitive-arcade-game",
    name: "Jogo De Fazer Highscore Altamente Competitivo",
    description: "the most cutthroat competitive arcade game i know of",
    // month: 11,
    year: 2018,
  },
  {
    id: "Brincadeira",
    name: "Brincadeira",
    // month: 11,
    year: 2018,
  },
  {
    id: "terra-nova",
    name: "Terra Nova",
    year:2018,
    collaborators: [
      c.ats, c.kosha, {name: "Guilherme Maculan"}
    ],
    month: 10,
    awards: [
      {
        title: `2nd place`,
        giver: `NASA Hackathon 2018`,
        month:10,
        year:2018,
      }
    ]
  },
  {
    id: "samsara",
    name: "Samsara",
    collaborators: [
      c.ats, c.kosha, c.nana, c.ubz, c.fabio, c.neder, {name: "cid"}, c.xinela
    ],
    // month:09,
    year:2018,
  },
  {
    id: "xinela",
    name: "atelie xinela",
    collaborators: [
      c.ats, c.kosha, c.nana, c.ubz, c.fabio, c.neder, {name: "cid"}, c.xinela
    ],
    year: 2018,
    press: [
      {
        outlet: "BRING",
        title: "BRING Talk #1",
        url: "https://www.youtube.com/watch?v=NLucRaBsskk"
      },
      {
        outlet: "correio brasiliense",
        title: "trabalhadores por tras dos games",
        url: "https://www.correiobraziliense.com.br/app/noticia/eu-estudante/trabalho-e-formacao/2019/06/10/interna-trabalhoeformacao-2019,761516/profissoes-que-estao-no-mercado-dos-games.shtml"
      },
      {
        outlet: "globo",
        title: "Ser Loki Holmes: jogo de carnaval dá pistas para folião descobrir data e local de festa no DF",
        url: "https://g1.globo.com/df/distrito-federal/carnaval/2019/noticia/2019/02/26/ser-loki-holmes-jogo-de-carnaval-da-pistas-para-foliao-descobrir-data-e-local-de-festa-no-df.ghtml"
      }
    ]
  },
  {
    id: "fofa-2018",
    name: "FOFA 2018",
    collaborators: [
      c.kosha,
    ],
    // month: 6,
    year:2018,
  },
  {
    id: "pabg",
    name: "Player Alone Battlegrounds",
    collaborators: [
      c.kosha, c.fabio
    ],
    // month: 4,
    year:2018,
    // https://www.youtube.com/watch?v=_B0k7sJIA5Y
    // https://www.youtube.com/watch?v=GR3UoErsqc
  },
  {
    id: "ampulheta",
    name: "Ampulheta",
    // month: 2,
    year: 2018,
  },
  {
    id: "infinite-panel",
    name: "Infinite Panel",
    // month: 2,
    year: 2018,
    description: "an infinite exquisite corpse inspired panel, that grows in all directions",
    url: "https://infinite-panel.herokuapp.com/"
  },
  {
    id: "good-vibes",
    name: "Good Vibes",
    collaborators: [
      { name: "Wagner Tavares Batista" }, { name: "Rafaias Mendes Ribeiro" }, { name: "José Alberto Gomes de Oliveira Karnikowski" }, { name: "Matheus Ferreira Ribeiro" }
    ],
    // month: 1,
    year:2018,
  },

  // y2017
  {
    id: 'ita',
    name: 'bsc computer engineering @ ITA',
    year: 2017,
    tags: "degree, software, engineering, programming",
    description: "i'm an computer engineer"
  },
  {
    id: 'balltris',
    name: 'balltris',
    year: 2017
  },
  {
    id: "pokemongo",
    name: "Poke Mongo",
    // month: 8,
    year: 2017,
  },
  {
    id: "synthballs",
    name: "Balltris",
    collaborators: [
      c.pai
    ],
    // month: 8,
    year: 2017,
  },
  {
    id: "crescimento",
    name: "Crescimento",
    // month: 6,
    year: 2017,
  },
  {
    id: "arvores",
    name: "Arvores",
    // month: 4,
    year: 2017,
  },
  {
    id: "subwars",
    name: "SubWars",
    // month: 1,
    year: 2017,
  },
  {
    id: "microsoft2017",
    name: "software development engineer intern @ Halo - 343 industries - microsoft",
    year: 2017,
    tags: "job, software engineering, programming, web development, tools, services, matchmaking, backend",
    description: "working on developing an internal web tool for analysing lobbies with the services team at 343, working in halo"
  },
  // -- y2016
  {
    id: "revolution-10",
    name: "Revolution 10",
    // month: 11,
    year: 2016,
  },
  {
    id: "monocular-rift",
    name: "Monocular Rift",
    // month: 8,
    year: 2016,
  },
  {
    id: "ddd1440",
    name: "DDD1440",
    collaborators: [
      {name: "Gabriel Amboss"},
    ],
    // month: 4,
    year:2016,
  },
  {
    id: "microsoft2016",
    name: "software development engineer intern @ bing ads - microsoft",
    year: 2016,
    end: 2016,
    tags: "job, software engineering, programming, web development, javascript, front end, react",
    description: "working on prototyping a migration of the bing ads billing web app from ASP.NET to modern javascript frameworks, at first react and then knockout.js"
  },
  // y2015
  {
    id: "monstah-time",
    name: "Monstah Time",
    tags: "ludum dare, local multiplayer, hidden role, top down",
    collaborators: [
      c.muzio, {name: "Paulo Aguiar"}
    ],
    description: "my first ever ludum dare, 33, a local multiplayer almost coop game, where one of the players know they are the monster and have to work against the group, but without being killed themselves",
    // month: 8,
    year:2015,
    url: "https://mutmedia.itch.io/monstah-time",
    itchId: 34702,
  },
  {
    id: "microsoft2015",
    name: "software development engineer intern @ bing ads - microsoft",
    year: 2016,
    tags: "job, software engineering, programming, sql, backend",
    description: "worked in improving performance on large queries and a refactoring project for using a new querying library on bing ads billing backend"
  },
  // y2014
  /// y2013
  {
    id: "dungeon-crawler-2013",

    name: "Dungeon Crawler",
    month: 6,
    year:2013,
    description: "the first game i've ever made, with XNA, long lost to switching computers, but i might try recovering it someday",
    url: "https://www.youtube.com/watch?v=euix3FZ4pNc",
  },
  {
    id: "elementalist",
    name: "The Elementalist",
    month: 10,
    year: 2013,
    description: "the first game jam i've ever participated with. play as a ragnarok online mage and control your skills properly to get the highest score possible",
    url: "https://www.youtube.com/watch?v=8OknZ_C0oZ8&list=LL&index=90"
  },
  // y2012
  // y2011
  // y2010
  // y2009
  // y2008
  // y2007
  // y2006
  // y2005
  // y2004
  {
    id: 'pptanimations',
    name: 'powerpoints animations',
    year: 2004,
    tags: 'animation, visual',
    description:
      'powerpoint animations I made in 2004, the first videogame adjacent content I remember to have made'
  },
  {
    id: 'born',
    name: 'being born',
    description: 'i don\'t really remember, but i guess it was hard',
    collaborators: [
      {name: "mae"}, c.pai
    ],
    // day: 10,
    // month: 6,
    year: 1995
  }
]

export default projects
